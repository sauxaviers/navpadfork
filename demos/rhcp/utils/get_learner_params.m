function learner_params = get_learner_params( id, grid_params )
%GET_LEARNER_PARAMS Summary of this function goes here
%   Detailed explanation goes here

learner_params = struct();

switch id
    case 1
        learner_params.get_collision_prob = @collision_prob_logreg;
        learner_params.update_learner = @update_log_reg;
        learner_params.feature = @(traj, robo_map) get_features(traj, robo_map, grid_params);
        learner_params.learning_rate = 0.0001;
        learner_params.weight = [ 7.9444   -0.0250   -0.7802    0 0   -2.0556];
    case 2
        learner_params.get_collision_prob = @collision_prob_logreg;
        learner_params.update_learner = @update_log_reg;
        learner_params.feature = @(traj, robo_map) get_features(traj, robo_map, grid_params);
        learner_params.learning_rate = 0.00001;
        learner_params.weight = [-9.9950    0.0731    0.0049   -0.0024    0.0004    0.0050];
    case 3
        learner_params.get_collision_prob = @collision_prob_logreg;
        learner_params.update_learner = @update_log_reg;
        learner_params.feature = @(traj, robo_map) get_features(traj, robo_map, grid_params);
        learner_params.learning_rate = 0.00001;
        learner_params.weight = zeros(1, 6);
end

end

