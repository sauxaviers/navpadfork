function [ V ] = get_value_function( W, goal_x, goal_y )
%GET_VALUE_FUNCTION Returns the value table given a map and goal
%   W is the map of the world (0 means occupied and 1 means unoccupied
%   goal_x goal_y vector specifying goal coordiante
%   V is a value table (size of W) that stores the euclidean distance of
%   every point from goal

g = [goal_x; goal_y];
options.nb_iter_max = Inf;
V = perform_fast_marching(double(W), g, options);

end

