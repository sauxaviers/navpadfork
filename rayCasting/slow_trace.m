function [xy,r,collision] = slow_trace(map,angle,max_range,position)

%%slow ray tracing in 2d

ranges = 0:0.5:max_range;
collision=0;
for i=1:max(size(ranges))
    xy = uint64(position + ranges(i)*[cos(angle),sin(angle)]);
     r=ranges(i);
        
    if(xy(1)<size(map,1) && xy(2)<size(map,2) && xy(1)>0 && xy(2)>0)
        
        if(map(xy(1),xy(2))>0)
            collision = 1;
            break;
        end
    end 
       
end
